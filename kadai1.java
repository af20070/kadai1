import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class kadai1 {
    public static void main(String[] args) {
        JFrame frame = new JFrame("kadai1");
        frame.setContentPane(new kadai1().top);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

        void order(String food) {
            int confirmation = JOptionPane.showConfirmDialog(null,
                    "Would you like to order " + food + "?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation == 0) {
                kane += price(food);
                Goukei.setText(kane+ " yen");
                String currentText = textPane1.getText();
                textPane1.setText(currentText + food +" "+price(food)+"yen"+"\n");
                JOptionPane.showConfirmDialog(null,
                        "Order for" + food + "received. Please wait!,",
                        "Order Confirmation",JOptionPane.CLOSED_OPTION);
            }
        }

    private JButton tempuraButton;
    private JButton karaageButton;
    private JButton gyozaButton;
    private JButton ramenButton;
    private JButton yakisobaButton;
    private JButton udonButton;
    private JTextPane textPane1;
    private JButton checkOutButton;
    private JPanel top;
    private JLabel Goukei;
    private JTree yen;
    int kane=0;
    int price(String food){
        if(food == "Tempura"){
            return 300;
        }
        else if(food == "Karaage"){
            return 200;
        }
        else if(food == "Gyoza"){
            return 100;
        }else if(food == "ramen"){
            return 500;
        }else if(food == "Yakisoba"){
            return 450;
        }
        else if(food == "Udon"){
            return 350;
        }
        return 0;
    }


    public kadai1() {


        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura");
            }
        });


        karaageButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) { order("Karaage");}
        });

        gyozaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Gyoza");}
        });

        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Udon");}
        });

        yakisobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("Yakisoba");}
        });

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {order("ramen");}
        });
        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to check out?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                   JOptionPane.showMessageDialog(null,
                            "Thank you!Have a good day",
                            "Checkout Message",
                            JOptionPane.YES_NO_OPTION);
                   textPane1.setText(" ");
                   kane = 0;
                   Goukei.setText(kane+" yen");

                }
            }

        });
    }


}
